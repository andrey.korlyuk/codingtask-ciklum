package com.codingtask;

import com.codingtask.model.Country;
import com.codingtask.model.CountryLookupRequest;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URI;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CountryLookupResourceTest extends JerseyTest {

    private static JsonObject searchResult;

    static {
        try {
            searchResult = Json.createReader(new FileReader("src/test/resources/searchResponse.json")).readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected Application configure() {
        return new ApplicationConfig();
    }

    @Override
    protected URI getBaseUri() {
        return UriBuilder.fromUri(super.getBaseUri()).path("countryservice").build();
    }


    @Test
    public void testExtractCountries() {
        CountryLoader loader = new CountryLoader();
        List<Country> countries = loader.extractCountries(searchResult);
        assertEquals(8, countries.size());
    }

    @Test
    public void testSearchCountries() {
        final Response response = target("api/search")
                .request(MediaType.APPLICATION_JSON).post(Entity.json(new CountryLookupRequest("be")));
        assertEquals(200, response.getStatus());
    }

    @Test
    public void testFailSearchCountries() {
        final Response response = target("api/search")
                .request(MediaType.APPLICATION_JSON).post(null);
        assertEquals(400, response.getStatus());
    }
}
