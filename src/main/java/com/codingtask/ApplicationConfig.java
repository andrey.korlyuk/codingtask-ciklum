package com.codingtask;

import com.codingtask.provider.JsonProvider;
import com.codingtask.provider.ServiceExceptionMapper;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;
import java.util.logging.Logger;

@ApplicationPath("/*")
public class ApplicationConfig extends Application {

    private static Logger log = Logger.getLogger(ApplicationConfig.class.getName());

    @Override
    public Set<Class<?>> getClasses() {

        Set<Class<?>> resources = new java.util.HashSet<>();

        log.fine("REST configuration starting: getClasses()");

        resources.add(org.glassfish.jersey.jackson.JacksonFeature.class);
        resources.add(JsonProvider.class);
        resources.add(ServiceExceptionMapper.class);
        resources.add(CountryLookupResource.class);

        log.fine("REST configuration ended successfully.");

        return resources;
    }
}
