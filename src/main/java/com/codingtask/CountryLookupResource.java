package com.codingtask;

import com.codingtask.model.CountryLookupRequest;
import com.codingtask.model.CountryLookupResponse;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Path("api")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CountryLookupResource {

    private LoadingCache<String, CountryLookupResponse> cache = CacheBuilder.newBuilder()
            .maximumSize(100)
            .weakKeys()
            .expireAfterWrite(10, TimeUnit.MINUTES).build(new CountryLoader());

    @POST
    @Path("/search")
    public CountryLookupResponse lookupCountry(final CountryLookupRequest request) {
        if(request == null || request.getName() == null) throw new WebApplicationException(Response.Status.BAD_REQUEST);

        try {
            return cache.get(request.getName());
        } catch (ExecutionException e) {
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }


}
