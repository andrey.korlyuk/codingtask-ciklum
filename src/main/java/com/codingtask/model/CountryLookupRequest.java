package com.codingtask.model;


public class CountryLookupRequest {

    private String name;

    @SuppressWarnings("unused")
    public CountryLookupRequest() {
    }

    public CountryLookupRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
