package com.codingtask.model;

import java.util.List;

public class CountryLookupResponse {
    private List<Country> result;

    @SuppressWarnings("unused")
    public CountryLookupResponse() {
    }

    public CountryLookupResponse(List<Country> result) {
        this.result = result;
    }

    public List<Country> getResult() {
        return result;
    }

}
