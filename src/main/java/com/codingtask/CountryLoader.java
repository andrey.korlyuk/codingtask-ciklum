package com.codingtask;

import com.codingtask.model.Country;
import com.codingtask.model.CountryLookupResponse;
import com.google.common.cache.CacheLoader;
import org.glassfish.jersey.jackson.JacksonFeature;

import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class CountryLoader extends CacheLoader<String, CountryLookupResponse> {

    private static final String LOOKUP_URL = "http://services.groupkt.com/country/search?text=";

    private final Client client = ClientBuilder.newBuilder()
            .register(JacksonFeature.class)
            .build();

    @Override
    public CountryLookupResponse load(String key) throws Exception {

        Response lookupResponse = client.target(LOOKUP_URL + key).request().get();

        if(lookupResponse.getStatus() != 200) throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);

        JsonObject responseJson = lookupResponse.readEntity(JsonObject.class);
        List<Country> countryList = extractCountries(responseJson);

        return new CountryLookupResponse(countryList);
    }

    List<Country> extractCountries(JsonObject jsonObject) {
        JsonArray countryArray = jsonObject.getJsonObject("RestResponse").getJsonArray("result");
        List<Country> countryList = new ArrayList<>();
        for(JsonObject r : countryArray.getValuesAs(JsonObject.class)){
            Country country = new Country(r.getString("name"), r.getString("alpha2_code"));
            countryList.add(country);
        }
        return countryList;
    }
}
